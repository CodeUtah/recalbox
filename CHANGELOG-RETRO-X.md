# Change Log
All notable changes to this project will be documented in this file (focus on change done on RETRO-X branch).

## [RETRO-X] - Beta 18 - 2021-10-13
- Pegasus :
	- Change version to "PEGASUS (pixL version)"
	- Improve Pegasus loading bar using more information and steps (including fixes now)
	- Display all controllers connected in menu Controllers
	- Fixes on GamepadEditor for new controllers
	- New bluetooth feature for controllers and more (as bluetooth speaker and headset):
		- Several methods of scanning/pairing and to forget devices
		- Show vendor name if available
		- Hide no name devices
		- Visibility of paired devices
		- Possibility to ignore devices
		- Forget and disconnect feature independently by device
		- Icon avaialble for several devices		
		- Show device status as paired/no success pairing and connected
		- Battery display (sony/nintendo devices)
	
- Theme (GameOS-pixL):
	- add play time, play count and last played in game info and also in settings
	- add new feature to change automatically favorites displayed in header of showcaseview	

- OS:
	- Add QT5 Connectivity dependencies for bluetooth features

## [RETRO-X] - Beta 17 - 2021-09-09
- fix SQLITE Drivers for QT (for Pegasus DB)
- Pegasus : 
	- fix to correct issue with "unknown" controllers by SDL
- GameOS theme:
	- performance and visibility optimization especially for main menu
	- Collections management (settings and display in 'main menu' collections for the moment)
	- arcade button in helps
	- gameinfo improvements
	
## [RETRO-X] - Beta 16 - 2021-08-28
- fix citra and pcsx2 not launch migration python3.9
- fix romfs v2 ps2
- bump configgen for fix citra and pcsx2
- bump configgen for fix citra and pcsx2
- bump pegasus for lastest fix/improvements:
	- fixes: fonts / helps / keyboard menu / logo selection in settings menu / binding improved / warning removed
	- Performance improvement on main menu

## [RETRO-X] - Beta 15 - 2021-08-09
- add ps2 and 3ds romfs v2 (bump configgen)
- "double" boot partition on pc x86-64 to prepare the future
- Pegasus:
	- popup for controllers connection including icons (ps/xbox & wheels)
	- new controller experience proposing to configure/test during first connection
	- several improvements in gamepad editor (colors, buttons, help)
	- Warning: 'step by step' not yet available but help instruction are displayed ;-)
	- New systemList.xml from romfs v2 supported
	- Fixes for issues on gameOS theme for default value from settings (as language and logos) - (#11,#13)
	- new API from Pegasus to manage list of parameters listed from system command.

## [RETRO-X] - Beta 14 - 2021-07-07
- set mame on default arcade emulator (#12)
- add pdfjs component in scripts directory as for gameos-pixl
- Pegasus:
		- fix keyboard qwerty retroachievement menu (and netplay) (#9)
		- retroachievements feature (including new gameos with new gameview retroachievements display)
		- logo optimization
		- pdfjs library from OS
		- rework menu and several fixes
		- link between languages of pegasus and systems
		- keyboard region management and change in real-time
		- add shaders selection in menu

## [RETRO-X] - Beta 13 - 2021-06-19
- Fix Ci gitlab (#6)
- Fix lags/skips during cinematics or in game mode 3DS citra-emu (#3)
- Fix fullscreen is never kept on 3DS Citra-emu (#5)
- Bump configgen :
    - fix L and R not work
- add gstreamer full plugins :
    - fix mute video preview in gameOs
    - fix many video preview not work

## [RETRO-X] - Beta 12 - 2021-05-25
- add nintendo 3ds citra emu:
    - add citra on verion nightly-1311
    - add recalbox romf package
    - change to citra qt and change repository
    - bump citra to nightly 1661
    - fix compilation mistake on defconfigs
    - bump configgen and citra emu
- bump last version of Pegasus (commit: 42c2... ):
    - including new translation in french for new menu
    - mouse fix in menu (with possibility to deactivate or not)
- bump last version of Configgen (commit: 609a...) for ratio improvement
- supermodel: remove option for video engine and fix ratio option / save states fix

## [RETRO-X] - Beta 11 - 2021-05-23
- add THEMES to manage themes directory from share
- bump last version of Pegasus (commit: 6f0d...) / come back to use commit and not branch name to avoid problem of build:
    - with best performance (Gamelist Only)
    - add hash management for theme as GameOS usage
- test version for Supermodel (new compilation/configgen)

## [change-frontend-to-pegasus] - Beta 10 "dirty" - 2021-04-29
- including PS2 test version

## [change-frontend-to-pegasus] - Beta 10 - 2021-04-22
- pegasus version with storage selection as "NETWORK, INTERNAL, etc.." and with sound management
    
## [change-frontend-to-pegasus] - Beta 9 - 2021-04-16
- new pegasus version:  
    - with new manual management and more media for our GameOS Theme fork ;-)
    - management of keyboard simulation with pad for browser browsing used in pdf manual display.
- based on recalbox 7.2 Beta 23 (with Mame 0.230... and other things)
- nvidia 460 drivers

## [change-frontend-to-pegasus] - Beta 8 - 2021-04-10
- add QT5 WEB ENGINE for future used ;-)

## [change-frontend-to-pegasus] - Beta 7 - 2021-04-02
- fix on S14migrate boot script: remove migration part about virtual keyboard
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - Beta 6 - 2021-04-01 (including change done for beta 1 to 5)
- fix(intel): disable VSYNC using "vblank_mode=0" to launch "pegasus-fe"
- kill pegasus if "es stop" command launched 
- add gstreamer for qt5multimedia
- add reboot and shutdown for buildroot (patch on pegasus)
- fix sound and video launch for pegasus
- change repository for pegasus (to use our fork finally)
- remove reboot and shutdown patch of pegasus
- add QT virtualkeyboard
- add qt5base_xml for xml and qt5quickcontrols for next popups
- use recalbox-integration branch for pegasus
- add qt5 quick controls 2 
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - 2021-02-03
- New branch created to integrate the new front-end for recalbox - Pegasus Front-end
- init package pegasus frontend
