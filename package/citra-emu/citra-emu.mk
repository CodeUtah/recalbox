################################################################################
#
# CITRA_EMU
#
################################################################################

# Version nightly-1711
CITRA_EMU_VERSION = 842031a2eb0b4bac4a351db914530296812962d1
CITRA_EMU_SITE = https://github.com/citra-emu/citra.git
CITRA_EMU_SITE_METHOD = git
CITRA_EMU_GIT_SUBMODULES = YES
CITRA_EMU_LICENSE = GPLv2
CITRA_EMU_DEPENDENCIES = sdl2 fmt ffmpeg boost qt5base qt5tools qt5multimedia
CITRA_EMU_SUPPORTS_IN_SOURCE_BUILD = NO

CITRA_EMU_CONF_OPTS += -DENABLE_QT=ON # Enable the Qt frontend
CITRA_EMU_CONF_OPTS += -DENABLE_CUBEB=ON # Enables the cubeb audio backend
CITRA_EMU_CONF_OPTS += -DENABLE_SDL2=ON
CITRA_EMU_CONF_OPTS += -DENABLE_WEB_SERVICE=ON # Enable web services (telemetry, etc.)
CITRA_EMU_CONF_OPTS += -DENABLE_QT_TRANSLATION=OFF # Enable translations for the Qt frontend
CITRA_EMU_CONF_OPTS += -DENABLE_FFMPEG=OFF
CITRA_EMU_CONF_OPTS += -DENABLE_FFMPEG_AUDIO_DECODER=OFF # "Enable FFmpeg audio (AAC) decoder" OFF)
CITRA_EMU_CONF_OPTS += -DENABLE_FFMPEG_VIDEO_DUMPER=OFF # "Enable FFmpeg video dumper" OFF)
CITRA_EMU_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
CITRA_EMU_CONF_OPTS += -DBUILD_SHARED_LIBS=FALSE
CITRA_EMU_CONF_OPTS += -DUSE_SYSTEM_BOOST=OFF # Use the system Boost libs (instead of the bundled ones)
CITRA_EMU_CONF_OPTS += -DENABLE_FDK=ON

CITRA_EMU_CONF_OPTS += -DCMAKE_C_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
CITRA_EMU_CONF_OPTS += -DCMAKE_C_ARCHIVE_FINISH=true
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_FINISH=true
CITRA_EMU_CONF_OPTS += -DCMAKE_AR="$(TARGET_CC)-ar"
CITRA_EMU_CONF_OPTS += -DCMAKE_C_COMPILER="$(TARGET_CC)"
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_COMPILER="$(TARGET_CXX)"
CITRA_EMU_CONF_OPTS += -DCMAKE_LINKER="$(TARGET_LD)"
CITRA_EMU_CONF_OPTS += -DCMAKE_EXE_LINKER_FLAGS="$(COMPILER_COMMONS_LDFLAGS_EXE)"

CITRA_EMU_CONF_ENV += LDFLAGS=-lpthread

define CITRA_EMU_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin
	mkdir -p $(TARGET_DIR)/usr/lib
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-qt \
		$(TARGET_DIR)/usr/bin/citra
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-room \
		$(TARGET_DIR)/usr/bin/citra-room
endef

$(eval $(cmake-package))
